package com.empty;


import net.sf.saxon.functions.Reverse;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        System.out.println("Введіть число від 0 до 100: ");
        Scanner scan = new Scanner(System.in);
        int numb1 = scan.nextInt();
        while (numb1 < 0 || numb1 > 100) {
            System.out.println("Ви неправильно ввели число! Спробуйте знову: ");
            numb1 = scan.nextInt();
        }

        System.out.printf("Введіть число від %d до 100: ", numb1);
        int numb2 = scan.nextInt();
        while (numb2 < numb1 || numb2 > 100) {
            System.out.println("Ви неправильно ввели число! Спробуйте знову: ");
            numb2 = scan.nextInt();
        }

        System.out.println("Непарні числа: ");
        for (int i = numb1; i < numb2; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
            System.out.println("\nПарні числа: ");
            for (int j = numb1; j < numb2; j++) {
                if (j % 2 == 0) {
                    System.out.print(j + " ");
                }
            }
        }
    }
}