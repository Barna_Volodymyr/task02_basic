package com.empty;

import java.util.Scanner;

public class Fibonacci {
    static int fibonacci(int n)
    {
        return (n<=2 ? 1 : fibonacci(n-1) + fibonacci(n-2));
    }
    public static void main(String[] args)
    {
        System.out.println("Введіть кількість чисел Фібоначчі: ");
        Scanner scan = new Scanner(System.in);
        int numb = scan.nextInt();
        for (int n=1; n<=numb; n++)
            System.out.print(fibonacci(n)+", ");
        System.out.println("...");
        for (int i = numb; i % 2 == 0;) {
            System.out.println("Найбільше парне число: " + i);
        }
    }
}
